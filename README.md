# Zaber MetaMorph driver

## About

This repository contains the source code for Zaber's plugin for the MetaMorph microscopy
software. See the FWEE Wiki to find instructions for building and releasing this code.

## Repository structure

1. Zaber/CustomerDriver*.h - Definitions related to building MetaMorph DLLs.
2. Zaber/ZaberDLL/* - Source for our plugin plus MetaMorph plugin infrastructure. Note this includes a copy of the Zaber C Core Serial library which was previously imported through a Subversion external; this is no longer kept in sync automatically.
