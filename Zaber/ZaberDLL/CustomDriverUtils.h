#pragma once

const int MAX_CMD_LENGTH = 64;
const int MAX_PORT_NAME = 32;

enum error_t {ERROR_NONE = 1, ERROR_SERIAL_COMMUNICATION, ERROR_WARNINGS};
enum homing_t {ALWAYS, IF_NECESSARY, NEVER};

extern TCHAR	szPortName[MAX_PORT_NAME];
extern int		iBaudRate;
extern int		iDeviceNumber;
extern int		iAxisX;
extern int		iAxisY;
extern homing_t eHomeOnStartup;

BOOL ParseConfigFile(TCHAR* pszPath);
BOOL QueryCommand(void* pHandle, za_reply* zReply, char* pszCommand, int iDevice, int iAxis);
BOOL QueryCommand(void* pHandle, za_reply* zReply, char* pszCommand, int iDevice, int iAxis, int iData);
error_t HomeStage(void* pHandle, int iDevice);
BOOL GetAxis(int iComponent, int* iAxis);
error_t CheckWarnings(void* pHandle, int iDevice);
void DisplayWarning();
void DisplayCommunicationError(TCHAR* pszTitle);
void DisplayConfigurationError(TCHAR* pszMessage, TCHAR* pszPath);
