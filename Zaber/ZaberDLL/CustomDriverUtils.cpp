// CustomDriverProc.cpp : Defines the entry point for the DLL application.
//
#include "stdafx.h"
#include <tchar.h>

#include "../CustomDriverDef.h"
#include "CustomDriverUtils.h"

#pragma warning(disable: 4996)

/**********************************************************
*                                                         *
*  Helper functions                                       *
*                                                         *
**********************************************************/

TCHAR	 szPortName[MAX_PORT_NAME];
int		 iBaudRate;
int		 iDeviceNumber;
int		 iAxisX;
int		 iAxisY;
homing_t eHomeOnStartup;

BOOL ParseConfigFile(TCHAR* pszPath)
{
	HANDLE hFile = CreateFile(pszPath,
		GENERIC_READ | GENERIC_WRITE, 
		0,                            
		NULL,                         
		OPEN_ALWAYS,                  
		FILE_ATTRIBUTE_NORMAL,        
		NULL);                        

	TCHAR szErrorMessage[512];

	if (hFile == INVALID_HANDLE_VALUE)
	{
		swprintf(szErrorMessage, __T("Could not access Zaber stage configuration file %s"), pszPath);
		MessageBox(
			NULL,
			szErrorMessage,
			NULL,
			MB_OK | MB_ICONERROR);
		return FALSE;
	}

	CloseHandle(hFile);

	// Get configuration values from MetaMorph.ini.
	// Use defaults (and write them to the file) if not found.
	if (!GetPrivateProfileString(__T("port"), __T("name"),
		NULL, szPortName, MAX_PORT_NAME, pszPath))
	{
		wcscpy(szPortName, __T("COM1"));
		WritePrivateProfileString(__T("port"), __T("name"), __T("COM1"), pszPath);
	}

	iBaudRate = GetPrivateProfileInt(__T("port"), __T("baudrate"), 0, pszPath);
	if (!iBaudRate)
	{
		iBaudRate = 115200;
		WritePrivateProfileString(__T("port"), __T("baudrate"), __T("115200"), pszPath);
	}

	// No validation for port name and baud rate since it will be done by za_serial.

	iDeviceNumber = GetPrivateProfileInt(__T("device"), __T("number"), 0, pszPath);
	if (!iDeviceNumber)
	{
		iDeviceNumber = 1;
		WritePrivateProfileString(__T("device"), __T("number"), __T("1"), pszPath);
	}

	if (iDeviceNumber < 1 || iDeviceNumber > 99)
	{
		swprintf(szErrorMessage, __T("Invalid device number \"%d\".\nValid device numbers are 1-99."), iDeviceNumber);
		DisplayConfigurationError(szErrorMessage, pszPath);
		return FALSE;
	}

	iAxisX = GetPrivateProfileInt(__T("device"), __T("axisX"), 0, pszPath);
	if (!iAxisX)
	{
		iAxisX = 2;
		WritePrivateProfileString(__T("device"), __T("axisX"), __T("2"), pszPath);
	}

	if (iAxisX < 1 || iAxisX > 9)
	{
		swprintf(szErrorMessage, __T("Invalid axis number \"%d\"."), iAxisX);
		DisplayConfigurationError(szErrorMessage, pszPath);
		return FALSE;
	}

	iAxisY = GetPrivateProfileInt(__T("device"), __T("axisY"), 0, pszPath);
	if (!iAxisY)
	{
		iAxisX = 1;
		WritePrivateProfileString(__T("device"), __T("axisY"), __T("1"), pszPath);
	}

	if (iAxisY < 1 || iAxisY > 9)
	{
		swprintf(szErrorMessage, __T("Invalid axis number \"%d\"."), iAxisY);
		DisplayConfigurationError(szErrorMessage, pszPath);
		return FALSE;
	}

	if (iAxisX == iAxisY)
	{
		DisplayConfigurationError(__T("Axis numbers cannot be identical."), pszPath);
		return FALSE;
	}

	TCHAR szHomeTemp[16];
	if (!GetPrivateProfileString(__T("MM"), __T("home on startup"), NULL, szHomeTemp, 16, pszPath))
	{
		wcscpy(szHomeTemp, __T("if necessary"));
		WritePrivateProfileString(__T("MM"), __T("home on startup"), __T("if necessary"), pszPath);
	}

	if (wcscmp(szHomeTemp, __T("always")) == 0)
	{
		eHomeOnStartup = ALWAYS;
	}
	else if (wcscmp(szHomeTemp, __T("if necessary")) == 0)
	{
		eHomeOnStartup = IF_NECESSARY;
	}
	else if (wcscmp(szHomeTemp, __T("never")) == 0)
	{
		eHomeOnStartup = NEVER;
	}
	else
	{
		swprintf(szErrorMessage, __T("Invalid home on startup value \"%s\".\nPossible values are \"always\", \"if necessary\", and \"never\"."), szHomeTemp);
		DisplayConfigurationError(szErrorMessage, pszPath);
		return FALSE;
	}

	return TRUE;
}

BOOL QueryCommand(void* pHandle, za_reply* zReply, char* pszCommand, int iDevice, int iAxis)
{
	char pszFullCommand[MAX_CMD_LENGTH];

	sprintf(pszFullCommand, "/%d %d %s\n", iDevice, iAxis, pszCommand);

	::OutputDebugStringA(pszFullCommand);

	char szReply[MAX_CMD_LENGTH];

	if (za_send(pHandle, pszFullCommand) < 0)
	{
		::OutputDebugString(__T("Zaber: QueryCommand(): error sending.\n"));
		return FALSE;
	}

	if (za_receive(pHandle, szReply, MAX_CMD_LENGTH) < 0)
	{
		::OutputDebugString(__T("Zaber: QueryCommand(): error receiving.\n"));
		return FALSE;
	}

	if (za_decode(zReply, szReply) < 0)
	{
		::OutputDebugString(__T("Zaber: QueryCommand(): error decoding.\n"));
		return FALSE;
	}

	// Account for checksumming, chop off checksum if present.
	if (strlen(zReply->response_data) >= 3)
	{
		int iExpectedColonIndex = strlen(zReply->response_data) - 3;
		if (zReply->response_data[iExpectedColonIndex] == ':')
		{
			zReply->response_data[iExpectedColonIndex] = '\0';
		}
	}

	return TRUE;
}

BOOL QueryCommand(void* pHandle, za_reply* zReply, char* pszCommand, int iDevice, int iAxis, int iData)
{
	char pszDataCommand[MAX_CMD_LENGTH];

	sprintf(pszDataCommand, "%s %d", pszCommand, iData);

	return QueryCommand(pHandle, zReply, pszDataCommand, iDevice, iAxis);
}

error_t HomeStage(void* pHandle, int iDevice)
{
	za_reply zReply;

	if (!QueryCommand(pHandle, &zReply, "tools findrange", iDevice, 0))
	{
		::OutputDebugString(__T("Failed Zaber stage command tools findrange.\n"));
		return ERROR_SERIAL_COMMUNICATION;
	}

	if (strcmp(zReply.warning_flags, "--") != 0)
	{
		error_t eReturn = CheckWarnings(pHandle, iDevice);
		if (eReturn == ERROR_WARNINGS)
		{
			DisplayWarning();
		}
		else if (eReturn == ERROR_SERIAL_COMMUNICATION)
		{
			return ERROR_SERIAL_COMMUNICATION;
		}
	}

	while (strcmp(zReply.device_status, "BUSY") == 0)
	{
		if (!QueryCommand(pHandle, &zReply, "", iDevice, 0))
		{
			::OutputDebugString(__T("Failed to get Zaber stage status.\n"));
			return ERROR_SERIAL_COMMUNICATION;
		}
		Sleep(100);
	}

	return ERROR_NONE;
}

BOOL GetAxis(int iComponent, int* iAxis)
{
	switch (iComponent)
	{
		case COMPONENT_STAGEX:
			*iAxis = iAxisX;
			return TRUE;

		case COMPONENT_STAGEY:
			*iAxis = iAxisY;
			return TRUE;

		default:
			return FALSE;
	}
}

error_t CheckWarnings(void* pHandle, int iDevice)
{
	za_reply zWarnings;
	if (!QueryCommand(pHandle, &zWarnings, "warnings clear", iDevice, 0))
	{
		return ERROR_SERIAL_COMMUNICATION;
	}

	if (strstr(zWarnings.response_data, "WV") != NULL
		|| strstr(zWarnings.response_data, "WT") != NULL)
	{
		return ERROR_WARNINGS;
	}

	return ERROR_NONE;
}

void DisplayWarning()
{
	MessageBox(
		NULL,
		__T("The Zaber stage supply voltage is out of range or system temperature is too high.\nPlease remedy the condition before continuing."),
		__T("Warning"),
		MB_OK | MB_ICONERROR);
}

void DisplayCommunicationError(TCHAR* pszTitle)
{
	MessageBox(
		NULL,
		__T("There was an error communicating with the Zaber stage.\nIs the stage connected?"),
		pszTitle,
		MB_OK | MB_ICONERROR);
}

void DisplayConfigurationError(TCHAR* pszMessage, TCHAR* pszPath)
{
	TCHAR szErrorMessage[512];
	swprintf(szErrorMessage, __T("%s\nProblem in file %s"), pszMessage, pszPath);
	MessageBox(
		NULL,
		szErrorMessage,
		__T("Invalid Zaber Configuration File"),
		MB_OK | MB_ICONERROR);
}