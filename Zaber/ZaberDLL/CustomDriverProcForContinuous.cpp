// CustomDriverProc.cpp : Defines the entry point for the DLL application.
//
#include "stdafx.h"
#include <tchar.h>

#include "..\CustomDriverDef.h"
#include "..\CustomDriverProc.h"
#include "CustomDriverUtils.h"

#pragma warning(disable: 4996)

/**********************************************************
*                                                         *
*  Continuous Component Control                           *
*                                                         *
**********************************************************/

BOOL CUSTOMDRIVER_DECL ContinuousGetPosition(void* pHandle, int iComponent, double* pdpPosition)
{
	::OutputDebugString(__T("Zaber: ContinuousGetPosition()\n"));

	if (pHandle == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	int iAxis;
	if (!GetAxis(iComponent, &iAxis))
	{
		return FALSE;
	}

	za_reply zReply;
	if (!QueryCommand(pHandle, &zReply, "get pos", iDeviceNumber, iAxis))
	{
		return FALSE;
	}

	*pdpPosition = atoi(zReply.response_data);
	return TRUE;
}

BOOL CUSTOMDRIVER_DECL ContinuousGetRange(void* pHandle, int iComponent, double* pdpMin, double* pdpMax)
{
	::OutputDebugString(__T("Zaber: ContinuousGetRange()\n"));

	if (pHandle == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	int iAxis;
	if (!GetAxis(iComponent, &iAxis))
	{
		return FALSE;
	}

	za_reply zReply;
	if (!QueryCommand(pHandle, &zReply, "get limit.min", iDeviceNumber, iAxis))
	{
		::OutputDebugString(__T("Command 'get limit.max' failed."));
		DisplayCommunicationError(__T("Failed to Get Range"));
		return FALSE;
	}

	if (strcmp(zReply.warning_flags, "--") != 0)
	{
		if (CheckWarnings(pHandle, iDeviceNumber) == ERROR_WARNINGS)
		{
			DisplayWarning();
		}
	}

	if (strcmp(zReply.response_data, "BADAXIS") == 0)
	{
		TCHAR szErrorMessage[512];
		swprintf(szErrorMessage, __T("Invalid axis number %s"), iAxis); // TODO is it possible to use config error fn?
		MessageBox(
			NULL,
			szErrorMessage,
			__T("Invalid Zaber Configuration File"),
			MB_OK | MB_ICONERROR);
		return FALSE;
	}

	*pdpMin = atoi(zReply.response_data);

	if (!QueryCommand(pHandle, &zReply, "get limit.max", iDeviceNumber, iAxis))
	{
		::OutputDebugString(__T("Command 'get limit.max' failed."));
		DisplayCommunicationError(__T("Failed to Get Range"));
		return FALSE;
	}

	if (strcmp(zReply.warning_flags, "--") != 0)
	{
		if (CheckWarnings(pHandle, iDeviceNumber) == ERROR_WARNINGS)
		{
			DisplayWarning();
		}
	}

	*pdpMax = atoi(zReply.response_data);

	return TRUE;
}

BOOL CUSTOMDRIVER_DECL ContinuousSetPosition(void* pHandle, int iComponent, double dpPosition)
{
	::OutputDebugString(__T("Zaber: ContinuousSetPosition()\n"));

	if (pHandle == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	double	dpMin, dpMax;

	if (ContinuousGetRange(pHandle, iComponent, &dpMin, &dpMax)) {
		if (dpPosition < dpMin) {
			dpPosition = dpMin;
		} else if (dpPosition > dpMax) {
			dpPosition = dpMax;
		}
	}

	int iAxis;
	if (!GetAxis(iComponent, &iAxis))
	{
		return FALSE;
	}

	za_reply zReply;
	if (!QueryCommand(pHandle, &zReply, "move abs", iDeviceNumber, iAxis, (int)dpPosition))
	{
		::OutputDebugString(__T("Command 'move abs' failed."));
		DisplayCommunicationError(__T("Failed to Set Position"));
		return FALSE;
	}

	if (strcmp(zReply.response_data, "PARKED") == 0)
	{
		MessageBox(
			NULL,
			__T("The Zaber stage cannot move because it is currently parked."),
			__T("Failed to Set Position"),
			MB_OK | MB_ICONERROR);
		return FALSE;
	}

	// Not checking warnings here because they get checked in the call to
	// ContinuousGetRange above and are unlikely to change in ways that matter.

	return TRUE;
}

BOOL CUSTOMDRIVER_DECL ContinuousGetUsage(void* pHandle, int iComponent, int* pnUsage)
{
	::OutputDebugString(__T("Zaber: ContinuousGetUsage()\n"));

	if (pHandle == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	switch (iComponent)
	{
		case COMPONENT_STAGEX:
			*pnUsage = COMPONENTUSAGE_STAGEX;
			return TRUE;

		case COMPONENT_STAGEY:
			*pnUsage = COMPONENTUSAGE_STAGEY;
			return TRUE;

		default:
			return FALSE;
	}
}
