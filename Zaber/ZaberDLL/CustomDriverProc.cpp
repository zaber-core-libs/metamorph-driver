// CustomDriverProc.cpp : Defines the entry point for the DLL application.
//
#include "stdafx.h"
#include <tchar.h>
#include <string>

#include <ShlObj.h>
#include <Shlwapi.h>
#pragma comment(lib, "Shlwapi.lib")

#include "..\CustomDriverDef.h"
#include "..\CustomDriverProc.h"
#include "CustomDriverUtils.h"

#pragma warning(disable: 4996)

using namespace std;

/**********************************************************
*                                                         *
*  Device Control                                         *
*                                                         *
**********************************************************/

/* Note: OpenDriver always returns TRUE because CloseDriver
 * is not called otherwise. If opening the driver fails, the
 * handle is set to INVALID_HANDLE_VALUE and subsequent attempts
 * to use the driver do nothing. */
BOOL CUSTOMDRIVER_DECL OpenDriver(void** ppHandle)
{
	::OutputDebugString(__T("Zaber: OpenDriver()\n"));

	*ppHandle = INVALID_HANDLE_VALUE;

	// Get path of configuration file MetaMorph.ini. If either MetaMorph.ini
	// or the Zaber Technologies directory doesn't exist, they will be created.
	TCHAR szPath[MAX_PATH];
	TCHAR szErrorMessage[512];

	if (!SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szPath)))
	{
		MessageBox(
			NULL,
			__T("Could not access %APPDATA%/"),
			NULL,
			MB_OK | MB_ICONERROR);
		return TRUE;
	}

	PathAppend(szPath, __T("Zaber Technologies"));

	if (!CreateDirectory(szPath, NULL))
	{
		if (GetLastError() != ERROR_ALREADY_EXISTS)
		{
			swprintf(szErrorMessage, __T("Could not create directory %s"), szPath);
			MessageBox(
				NULL,
				szErrorMessage,
				NULL,
				MB_OK | MB_ICONERROR);
			return TRUE;
		}
	}

	PathAppend(szPath, __T("MetaMorph.ini"));

	if (!ParseConfigFile(szPath))
	{
		// ParseConfigFile makes its own error MessageBoxes.
		return FALSE;
	}

	wstring strPortName(szPortName); // for conversion to cstring with \\.\ prefix
	string strRobustPortName = "\\\\.\\" + string(strPortName.begin(), strPortName.end());
	z_port pHandle;
	if (za_connect(&pHandle, strRobustPortName.c_str()) != Z_SUCCESS)
	{
		swprintf(szErrorMessage, __T("Failed to connect to the Zaber stage on %s.\nPlease ensure the stage is connected and the correct port name is specified in %s"), szPortName, szPath);
		MessageBox(
			NULL,
			szErrorMessage,
			NULL,
			MB_OK | MB_ICONERROR);
		return TRUE;
	}

	if (iBaudRate != 115200) // za_connect opens the port with 115200
	{
		int iReturn = za_setbaud(pHandle, iBaudRate);
		if (iReturn == Z_ERROR_INVALID_BAUDRATE)
		{
			swprintf(szErrorMessage, __T("Invalid port baud rate \"%d\".\nValid baud rates are 9600, 19200, 38400, 57600, and 115200."), iBaudRate);
			DisplayConfigurationError(szErrorMessage, szPath);
			za_disconnect(pHandle);
			return TRUE;
		}
		else if (iReturn != Z_SUCCESS)
		{
			swprintf(szErrorMessage, __T("Failed to set %s baud rate to %d."), szPortName, iBaudRate);
			MessageBox(
				NULL,
				szErrorMessage,
				NULL,
				MB_OK | MB_ICONERROR);
			
			za_disconnect(pHandle);
			return TRUE;
		}
	}

	BOOL bHomeStage = FALSE;

	if (eHomeOnStartup == ALWAYS)
	{
		bHomeStage = TRUE;
	}
	else if (eHomeOnStartup == IF_NECESSARY)
	{
		za_reply zReply;
		if (!QueryCommand(pHandle, &zReply, "warnings", iDeviceNumber, 0))
		{
			DisplayCommunicationError(__T("Homing Error"));
			za_disconnect(pHandle);
			return TRUE;
		}

		if (strstr(zReply.response_data, "WR") != NULL)
		{
			bHomeStage = TRUE;
		}
	}

	if (bHomeStage)
	{
		if (HomeStage(pHandle, iDeviceNumber) == ERROR_SERIAL_COMMUNICATION)
		{
			DisplayCommunicationError(__T("Homing Error"));
			za_disconnect(pHandle);
			return TRUE;
		}
	}

	*ppHandle = pHandle;
	return TRUE;
}

BOOL CUSTOMDRIVER_DECL CloseDriver(void* pHandle)
{
	::OutputDebugString(__T("Zaber: CloseDriver()\n"));

	za_disconnect(pHandle);

	return TRUE;
}

BOOL CUSTOMDRIVER_DECL GetNumberOfComponents(void* pDriver, int nComponentType, int* pnNumberOfComponents)
{
	//if (pDriver == INVALID_HANDLE_VALUE)
	//{
	//	// No components will be created.
	//	return FALSE;
	//}

	if (nComponentType == COMPONENTTYPE_CONTINUOUS)
	{
		*pnNumberOfComponents = 2;
	}
	else
	{
		*pnNumberOfComponents = 0;
	}

	return TRUE;
}

BOOL CUSTOMDRIVER_DECL	GetComponentName(void* pDriver, int nComponentType, int iComponent, char* pszBuffer, int cbBuffer)
{
	const char*	pszName = "";

	if (nComponentType == COMPONENTTYPE_CONTINUOUS)
	{
		switch (iComponent)
		{
			case COMPONENT_STAGEX:
				pszName = "Zaber Stage X";
				break;

			case COMPONENT_STAGEY:
				pszName = "Zaber Stage Y";
				break;
		}
	}

	strncpy(pszBuffer, pszName, cbBuffer);

	return strlen(pszName) > 0;
}
