#pragma once

/******************************************************************************
* GetNumberOfComponents()/GetComponentName() Parameters
******************************************************************************/
const int	COMPONENTTYPE_CONTINUOUS			= 1;
const int	COMPONENTTYPE_DISCRETE				= 2;	//Components of this type are not currently created.
const int	COMPONENTTYPE_SHUTTER				= 3;
const int	COMPONENTTYPE_SLIDELOADER			= 4;

/******************************************************************************
* GetNumberOfComponents() Parameters
******************************************************************************/
const int	COMPONENTUSAGE_ILLUMINATION			= 1;
const int	COMPONENTUSAGE_STAGEX				= 2;
const int	COMPONENTUSAGE_STAGEY				= 3;
const int	COMPONENTUSAGE_ZMOTOR				= 4;
const int	COMPONENTUSAGE_LASER				= 5;

/******************************************************************************
* GetComponentName()/CustomDriverProcForContinuous.cpp Parameters
******************************************************************************/
const int	COMPONENT_STAGEX					= 0;
const int	COMPONENT_STAGEY					= 1;

/******************************************************************************
* SlideLoaderGetParameterValue(SLIDELOADER_PARAM_STATUS) return flags
******************************************************************************/
const DWORD	SLIDELOADER_STATUS_ERROR			= 0x00000001;

//Logic errors
const DWORD	SLIDELOADER_STATUS_INVALIDCASSETTE	= 0x00000010;
const DWORD	SLIDELOADER_STATUS_INVALIDSLIDE		= 0x00000020;
const DWORD	SLIDELOADER_STATUS_SLIDEONSTAGE		= 0x00000040;
const DWORD	SLIDELOADER_STATUS_NOSLIDE			= 0x00000080;
																	
//State errors														
const DWORD	SLIDELOADER_STATUS_NOTCONNECTED		= 0x00000100;
const DWORD	SLIDELOADER_STATUS_NOTINITIALIZED	= 0x00000200;
const DWORD	SLIDELOADER_STATUS_ISBUSY			= 0x00000300;
																	
//Motion errors														
const DWORD	SLIDELOADER_STATUS_MOTIONPROBLEM	= 0x00010000;

/******************************************************************************
* SlideLoaderGetParameterValue(SLIDELOADER_PARAM_IS_CASSETTE_PRESENT) return states
* SlideLoaderGetParameterValue(SLIDELOADER_PARAM_IS_SLIDE_PRESENT)
******************************************************************************/
//Presence states
const long	SLIDELOADER_PRESENT_STATE_UNKNOWN	= 0;
const long	SLIDELOADER_PRESENT_STATE_YES		= 1;
const long	SLIDELOADER_PRESENT_STATE_NO		= 2;

/******************************************************************************
* SlideLoaderGetParameterValue(SLIDELOADER_PARAM_SLOT_ORDER) return states
******************************************************************************/
//Slot order types
const long	SLIDELOADER_SLOT_ORDER_UNKNOWN		= 0;
const long	SLIDELOADER_SLOT_ORDER_ASCENDING	= 1;
const long	SLIDELOADER_SLOT_ORDER_DESCENDING	= 2;

/******************************************************************************
* SlideLoaderGetParameterValue(SLIDELOADER_PARAM_STAGE_TYPE) return states
******************************************************************************/
//Stage types
const long	SLIDELOADER_STAGE_TYPE_UNKNOWN		= 0;
const long	SLIDELOADER_STAGE_TYPE_FIXED		= 1;
const long	SLIDELOADER_STAGE_TYPE_MOVEABLE		= 2;

/******************************************************************************
* SlideLoaderGetParameterValue Parameters (Manditory)
******************************************************************************/
//The following parameters must be implemented.

const long	SLIDELOADER_PARAM_MAX_CASSETTES					= 1;	//args=none, return 1..4
const long	SLIDELOADER_PARAM_SLOTS_PER_CASSETTE			= 2;	//args=none, return 1..25
const long	SLIDELOADER_PARAM_STATUS						= 3;	//args=none, return SLIDELOADER_STATUS_ constant

/******************************************************************************
* SlideLoaderGetParameterValue Parameters (Optional)
******************************************************************************/
//The following parameters are optional. They do not have to be supported but it
//is suggested that they are so that the user interface behavior is well defined.

const long	SLIDELOADER_PARAM_IS_CASSETTE_PRESENT			= 11;	//arg1=cassette number (0-based), return SLIDELOADER_PRESENT_STATE_ constant
const long	SLIDELOADER_PARAM_IS_SLIDE_PRESENT				= 12;	//arg1=cassette number, arg2=slot number (0-based), return SLIDELOADER_PRESENT_STATE_ constant
const long	SLIDELOADER_PARAM_IS_STAGE_WAYPOINT_REQUIRED	= 13;	//args=none, return 0=false, non-0=true
const long	SLIDELOADER_PARAM_SLOT_ORDER					= 14;	//args=none, return SLIDELOADER_SLOT_ORDER_ constant
const long	SLIDELOADER_PARAM_STAGE_TYPE					= 15;	//args=none, return SLIDELOADER_STAGE_TYPE_ constant

/******************************************************************************
* SlideLoaderGetParameterStringValue Parameters (Optional)
******************************************************************************/
//The following parameters are optional.

const long	SLIDELOADER_PARAM_STRING_BARCODE				= 50;	//args=none, return barcode string

/******************************************************************************
* SlideLoaderCallFunction Parameters (Manditory)
******************************************************************************/
//The following parameters must be implemented.

//Both of these functions are asynchronous. Error detection and move completion determined
//by polling calls to SlideLoaderGetParameterValue(SLIDELOADER_PARAM_STATUS).
const long	SLIDELOADER_FUNCTION_MOVE_FROM_HOLDER_TO_STAGE	= 1;	//arg1=cassette number, arg2=slot number (0-based), return 0=failure, non-0=success
const long	SLIDELOADER_FUNCTION_MOVE_FROM_STAGE_TO_HOLDER	= 2;	//arg1=cassette number, arg2=slot number (0-based), return 0=failure, non-0=success

/******************************************************************************
* SlideLoaderCallFunction Parameters (Optional)
******************************************************************************/
//The following parameters are optional.

//Asynchronous initialization of the slide loader. Error detection and move completion determined
//by polling calls to SlideLoaderGetParameterValue(SLIDELOADER_PARAM_STATUS).
const long	SLIDELOADER_FUNCTION_INITIALIZE					= 11;	//args=none, return 0=failure, non-0=success

//Asynchronous homing of the slide loader. Error detection and move completion determined
//by polling calls to SlideLoaderGetParameterValue(SLIDELOADER_PARAM_STATUS).
const long	SLIDELOADER_FUNCTION_HOME						= 12;	//args=none, return 0=failure, non-0=success

//Error detection and move completion determined by polling calls to SlideLoaderGetParameterValue(SLIDELOADER_PARAM_STATUS).
const long	SLIDELOADER_FUNCTION_MOVE_FROM_HOLDER_TO_HOLDER	= 13;	//arg1=cassette number 1, arg2=slot number 1, arg3=cassette number 2, arg4=slot number 2 (0-based), return 0=failure, non-0=success

//Asynchronous detection of slides within a cassette. Error detection and move completion determined
//by polling calls to SlideLoaderGetParameterValue(SLIDELOADER_PARAM_STATUS).
const long	SLIDELOADER_FUNCTION_SCAN_CASSETTE				= 14;	//arg1=cassette number (0-based), return 0=failure, non-0=success

//Halt motion of all axes.
const long	SLIDELOADER_FUNCTION_HALT						= 15;	//args=none, return 0=failure, non-0=success.

//If available then calls are made to instruct the slide loader to assume that a specified slide is
//present.  This is necessary for some hardware to initiate moves without the need for scanning.
const long	SLIDELOADER_FUNCTION_FORCE_SLIDE_PRESENT		= 16;	//arg1=cassette number, arg2=slot number (0-based)

//Synchronous dispensing of oil.
const long	SLIDELOADER_FUNCTION_DISPENSE_OIL				= 17;	//args=none, return 0=failure, non-0=success.

//NOTES:
//1. All cassette and slot indexers are 0-based.
//2. Function and parameter availability states are not allowed to change.