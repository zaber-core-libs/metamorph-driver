#pragma once

#define CUSTOMDRIVER_CONV	WINAPI
#define CUSTOMDRIVER_DECL	__declspec(dllexport) CUSTOMDRIVER_CONV

//Device Control
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_OPENDRIVER)(void** ppHandle);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_CLOSEDRIVER)(void* pDriver);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_GETNUMBEROFCOMPONENTS)(void* pDriver, int nComponentType, int* pnNumberOfComponents);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_GETCOMPONENTNAME)(void* pDriver, int nComponentType, int iComponent, char* pszBuffer, int cbBuffer);

//Continuous Component Control
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_CONTINUOUSGETPOSITION)(void* pDriver, int iComponent, double* pdpPosition);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_CONTINUOUSSETPOSITION)(void* pDriver, int iComponent, double dpPosition);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_CONTINUOUSGETRANGE)(void* pDriver, int iComponent, double* pdpMin, double* pdpMax);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_CONTINUOUSGETUSAGE)(void* pDriver, int iComponent, int* pnUsage);

//Shutter Component Control
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_SHUTTERGETPOSITION)(void* pDriver, int iComponent, BOOL* pbOpen);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_SHUTTERSETPOSITION)(void* pDriver, int iComponent, BOOL bOpen);

//Slide Loader Component Control
typedef BOOL (CUSTOMDRIVER_CONV *PFN_SLIDELOADERCALLFUNCTION)(void* pDriver, long idFunction, long arg1, long arg2, long arg3, long arg4);
typedef	BOOL (CUSTOMDRIVER_CONV *PFN_SLIDELOADERGETPARAMETERSTRINGVALUE)(void* pDriver, long idParameter, long arg1, long arg2, char* pszBuffer, int cbBuffer);
typedef BOOL (CUSTOMDRIVER_CONV *PFN_SLIDELOADERGETPARAMETERVALUE)(void* pDriver, long idParameter, long arg1, long arg2, long* pValue);
typedef BOOL (CUSTOMDRIVER_CONV *PFN_SLIDELOADERISFUNCTIONAVAILABLE)(void* pDriver, long idFunction);
typedef BOOL (CUSTOMDRIVER_CONV *PFN_SLIDELOADERISPARAMETERAVAILABLE)(void* pDriver, long idParameter);

#ifdef EXPORT_CUSTOM_DRIVER_FUNCTIONS

extern "C" {
	//Device Control
	BOOL CUSTOMDRIVER_DECL	OpenDriver(void** ppHandle);
	BOOL CUSTOMDRIVER_DECL	CloseDriver(void* pHandle);
	BOOL CUSTOMDRIVER_DECL	GetNumberOfComponents(void* pDriver, int nComponentType, int* pnNumberOfComponents);
	BOOL CUSTOMDRIVER_DECL	GetComponentName(void* pDriver, int nComponentType, int iComponent, char* pszBuffer, int cbBuffer);

	//Continuous Component Control
	BOOL CUSTOMDRIVER_DECL	ContinuousGetPosition(void* pHandle, int iComponent, double* pdpPosition);
	BOOL CUSTOMDRIVER_DECL	ContinuousSetPosition(void* pHandle, int iComponent, double dpPosition);
	BOOL CUSTOMDRIVER_DECL	ContinuousGetRange(void* pHandle, int iComponent, double* pdpMin, double* pdpMax);
	BOOL CUSTOMDRIVER_DECL	ContinuousGetUsage(void* pHandle, int iComponent, int* pnUsage);

	//Shutter Component Control
	BOOL CUSTOMDRIVER_DECL	ShutterGetPosition(void* pHandle, int iComponent, BOOL* pbOpen);
	BOOL CUSTOMDRIVER_DECL	ShutterSetPosition(void* pHandle, int iComponent, BOOL bOpen);

	//Slide Loader Component Control
	BOOL CUSTOMDRIVER_DECL	SlideLoaderCallFunction(void* pHandle, long idFunction, long arg1, long arg2, long arg3, long arg4);
	BOOL CUSTOMDRIVER_DECL	SlideLoaderGetParameterStringValue(void* pHandle, long idParameter, long arg1, long arg2, char* pszBuffer, int cbBuffer);
	BOOL CUSTOMDRIVER_DECL	SlideLoaderGetParameterValue(void* pHandle, long idParameter, long arg1, long arg2, long* pValue);
	BOOL CUSTOMDRIVER_DECL	SlideLoaderIsFunctionAvailable(void* pHandle, long idFunction);
	BOOL CUSTOMDRIVER_DECL	SlideLoaderIsParameterAvailable(void* pHandle, long idParameter);
};

#endif